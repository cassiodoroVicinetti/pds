\chapter{Programmazione concorrente in C++11}
\section{Introduzione alla programmazione concorrente}
%3
\paragraph{Programmazione sequenziale} Ogni \textbf{processo} contiene un solo flusso di esecuzione che si svolge con un comportamento deterministico.

Processi concorrenti possono comunicare tra loro solo attraverso apposite zone di memoria condivise o primitive per la comunicazione tra processi offerte dal sistema operativo (si rimanda al capitolo~\ref{cap:ipc}), che sono delle soluzioni generalmente lente e poco efficienti perché i processi hanno \ul{spazi di indirizzamento separati}.

%4-20-21
\paragraph{Programmazione concorrente} Ogni processo contiene più flussi di esecuzione, uno per ogni \textbf{thread}, che si svolgono contemporaneamente con un comportamento non deterministico (dipendente dalla politica dello scheduler).

%Schedulare due thread appartenenti a due processi diversi richiede di reinizializzare tutto il sistema di memoria (pagine virtuali di un processo non corrispondono alle pagine di un altro).

%5
\paragraph{Vantaggi della programmazione concorrente}
\begin{itemize}
  \item \ul{stesso spazio di indirizzamento}: i thread all'interno di uno stesso processo condividono lo stesso spazio di indirizzamento:
  \begin{itemize}
    \item \ul{inter-comunicazione efficiente}: la comunicazione tra thread dello stesso processo avviene in maniera più efficiente rispetto alla comunicazione tra processi, in quanto i dati possono essere condivisi evitando copie onerose da uno spazio di indirizzamento all'altro;
    \item \ul{commutazione di contesto efficiente}: la commutazione di contesto tra thread dello stesso processo avviene in maniera più efficiente rispetto alla commutazione di contesto tra processi, in quanto non richiede la rimappatura dell'area di memoria;
  \end{itemize}
    
  \item \ul{operazioni di I/O asincrone}: mentre un thread si pone in attesa di I/O, un altro thread può eseguire altre parti dell'algoritmo;
  
  \item \ul{vero parallelismo}: se si dispone di CPU multi-core, più flussi di esecuzione possono svolgersi contemporaneamente, riducendo il tempo totale di elaborazione.\\
  I thread vengono eseguiti effettivamente in parallelo se la CPU dispone di un numero di core sufficiente, altrimenti sono eseguiti in alternanza in modo non deterministico.
\end{itemize}

%6-18-25-27-29-30-31-32-33-34-35-39-40-44-45-46
\paragraph{Svantaggi della programmazione concorrente}
L'accesso concorrente a dati in zone di memoria condivise può causare delle \textbf{interferenze} $\Rightarrow$ l'accesso alle zone di memoria condivise deve essere coordinato attraverso opportuni costrutti di \ul{sincronizzazione}:
\begin{itemize}
  \item possono crearsi dei \ul{deadlock} quando due thread si attendono a vicenda;
  \item gli \ul{errori} nell'uso dei costrutti di sincronizzazione possono manifestarsi in maniera \ul{casuale} a causa del comportamento non deterministico, rendendone il debug più complesso.
\end{itemize}

% \subsection{Garantire la correttezza}
% \subsubsection{Oggetti condivisi mutabili}
% Un thread dovrebbe sempre operare su un oggetto condiviso in maniera esclusiva. 
% Dunque \`e necessario che tutti gli oggetti condivisi mutabili godano di questa propriet\`a.
% 
% \subsubsection{Accesso condiviso: le cause del problema}
% \begin{itemize}
% \item Atomicit\`a: quali operazioni di memoria hanno effetti indivisibili?
% \item Visibilit\`a: sotto quali condizioni la scrittura di una variabile da parte di un thread pu\`o essere osservata da una lettura eseguita da un altro thread?
% \item Ordinamento: sotto quali condizioni sequenze di operazioni effettuate da un thread sono visibili nello stesso ordine da parte di altri thread?
% \end{itemize}
% 
% \subsubsection{C/C++ non garantiscono}
% \begin{itemize}
% \item Accesso non sincronizzato ad un dato
% \item Accesso ad un dato mentre una modifica \`e in corso
% \item Ri-ordinamento delle istruzioni
% \end{itemize}

\paragraph{Costrutti di sincronizzazione}
%28
\begin{itemize}
  \item UNIX:
  \begin{itemize}
    \item libreria POSIX Threads (Pthreads): mutex, condition;
    \item oggetti kernel: semafori, pipe, segnali;
  \end{itemize}
  
  \item Win32: (capitoli~\ref{cap:ipc} e~\ref{cap:sincronizzazione_win32})
  \begin{itemize}
    \item CriticalSection
    \item ConditionVariable
    \item oggetti kernel: Mutex, Event, Semaphore, Pipe, Mailslot, ecc.;
  \end{itemize}
  
  %47-59
  \item C++11:
  \begin{itemize}
    \item approccio di alto livello: basato su \texttt{async} e \texttt{future};
    \item approccio di basso livello: uso esplicito di thread e costrutti di sincronizzazione.
  \end{itemize}
  %A partire dalla versione C++ il concetto di thread ale dello standard del linguaggio, facilitando la portabilit`a.
\end{itemize}

\section{Thread}
%60
Un oggetto \texttt{thread} modella un thread del sistema operativo.

La classe \texttt{thread} implementa i seguenti metodi:
\begin{itemize}
%61
\item costruttore: crea un nuovo thread e chiama l'oggetto Callable specificato;
%63
\item funzione \texttt{join()}: il thread chiamante si blocca in attesa che il thread specificato termini;
\item funzione \texttt{detach()}: il thread chiamante non è interessato ad attendere la terminazione del thread specificato.
%l'oggetto thread sul quale \`e chiamato il metodo si distacca dal flusso di elaborazione corrispondente.\\
\end{itemize}

%62-63-67
Se il thread principale termina normalmente ritornando dal \texttt{main()}, l'intero processo termina, insieme a tutti i suoi thread secondari, e tutti i distruttori sono chiamati. In altri casi l'intero processo termina senza che vengano chiamati i distruttori $\Rightarrow$ ciò può portare a perdite di memoria:
  \begin{itemize}
    \item se in un thread secondario si verifica un'eccezione non gestita;
    \item se viene distrutto un oggetto \texttt{thread} senza che siano state chiamate le funzioni \texttt{join()} o \texttt{detach()};
    \item se il thread principale termina per cause diverse dal ritorno dal \texttt{main()}.
  \end{itemize}

%69
\paragraph{Spazio dei nomi \texttt{this\_thread}}
Offre delle funzioni relative al thread chiamante:
\begin{itemize}
 \item funzione \texttt{this\_thread::get\_id()}: restituisce l'identificativo del thread chiamante;
 \item funzione \texttt{this\_thread::sleep\_for()}: sospende l'esecuzione del thread chiamante per \ul{almeno} il tempo specificato (\texttt{chrono::duration});
 \item funzione \texttt{this\_thread::sleep\_until()}: interrompe l'esecuzione del thread chiamante \ul{almeno} fino al momento specificato (\texttt{chrono::time\_point});
 \item funzione \texttt{this\_thread::yield()}: il thread chiamante offre volontariamente l'esecuzione a un eventuale altro thread.
\end{itemize}

%48
\section{Esecuzione asincrona}
\subsection{Future}
Un oggetto \texttt{future} rappresenta un risultato che non è disponibile al momento della sua creazione, ma che sarà reso disponibile in futuro.

La classe \texttt{future} implementa i seguenti metodi:
\begin{itemize}
%49-53
\item funzione \texttt{get()}: aspetta che il risultato sia pronto e ne restituisce il valore (o lancia l'eventuale eccezione), e può essere chiamata una sola volta;
\item funzione \texttt{wait()}: aspetta che il risultato sia pronto \ul{senza} prelevarne il valore, e può essere chiamata più volte.\\
Le funzioni \texttt{get()} e \texttt{wait()}:
\begin{itemize}
    \item se l'esecuzione è terminata, ritornano subito;
    \item se l'esecuzione è ancora in corso, si bloccano in attesa che finisca;
    \item se l'esecuzione non è ancora iniziata, ne forzano l'avvio nel thread corrente e si bloccano in attesa che finisca;
\end{itemize}
%54
\item funzioni \texttt{wait\_for()} e \texttt{wait\_until()}: aspettano rispettivamente per il tempo specificato e fino al momento specificato:
\begin{itemize}
    \item se l'esecuzione è terminata, restituiscono \texttt{future\_status::ready};
    \item se l'esecuzione è ancora in corso, restituiscono \texttt{future\_status::timeout};
    \item se l'esecuzione non è ancora iniziata, restituiscono \texttt{future\_status::deferred};
\end{itemize}
    
%56-57
\item funzione \texttt{share()}: permette di ottenere un oggetto \texttt{shared\_future} a partire da un oggetto \texttt{future}, in modo che possa essere valutato pi\`u volte (cioè si possano effettuare pi\`u \texttt{get()} su di esso).\\
Un oggetto \texttt{shared\_future} \`e copiabile e movibile.
\end{itemize}

Quando un oggetto \texttt{future} viene distrutto, se la computazione \`e ancora attiva il distruttore ne attende la fine.

\subsection{Promise}
promise<T>
\begin{itemize}
\item get\_future()
\begin{Cpp}
  void f(promise<string>& p) {
    ...
  }

  int main() {
    promise<string> p;
    /* Creazione di un thread responsabile dell'elaborazione */
    thread t(f, ref(p)); //forza p ad essere passata per reference t.detach();
    ...
    /* Attesa del risultato */
    string result = p.get_future().get();
  }
\end{Cpp}
\end{itemize}

\subsection{Async}
%49
Il metodo \texttt{async()} esegue un oggetto chiamabile in maniera asincrona e ritorna un \texttt{future<T>}, dove \texttt{T} è il tipo del valore di ritorno dell'oggetto chiamabile:
\begin{Cpp}
  future<<T>> future1 = async(function1, "...", 3.14);
  string res1 = future1.get();
\end{Cpp}

%51
L'oggetto chiamabile pu\`o essere preceduto da una costante che definisce la politica di attivazione:
\begin{itemize}
 \item \texttt{launch::async}: attiva un thread secondario che esegue l'operazione;
 \item \texttt{launch::deferred}: l'operazione non viene eseguita finché non vengono chiamati i metodi \texttt{get()} o \texttt{wait()} sul future relativo (lazy evalutation).\\
 Per supportare questo tipo di comportamento, evitando molteplici chiamate su un metodo lazy, \`e possibile usare la classe \texttt{once\_flag} e la funzione \texttt{call\_once()}.
\end{itemize}

%function1 pu\`o essere o un puntatore a funzione o un oggetto chiamabile (che ha definito al suo interno un operator)

\section{Sincronizzazione}
\begin{itemize}
\item mutex
\item recursive\_mutex
\item timed\_mutex
\item recursive\_timed\_mutex
\item lock\_guard<Lockable> (lock\_guard<mutex>)
\item unique\_lock<Lockable> (unique\_lock<mutex>)
  \begin{itemize}
  \item adopt\_lock
  \item defer\_lock
  \item try\_lock
  \end{itemize}
\item condition\_variable (richiede l'uso di un unique\_lock<mutex> per garantire l'accesso in mutua esclusione alle variabili usate per valutare la condizione di attesa).
  \begin{itemize}
  \item wait(unique\_lock)
  \item wait\_for()
  \item wait\_until()
  \item notify\_one()
  \item notify\_all()
  \end{itemize}
\end{itemize}

\underline{Nota}:
 attenzione ai risvegli spuri.

\subsection{Operazioni atomiche}
La classe atomic<T> offre la possibilit\`a di accedere in modo atomico al tipo T.

\section{Singleton pattern e Lazy evaluation}
\begin{itemize}
\item once\_flag:
  struttura di appoggio per la funzione call\_once.
\item call\_once(flag, function, ...):
  esegue function una sola volta (se la chiamata \`e gi\`a terminata ritorna immediatamente).
\end{itemize}

\begin{Cpp}
  #include <mutex>
  class Singleton {
    static Singleton *instance; static once_flag inited;
    Singleton() {...} //privato
    public:
    static Singleton *getInstance() {
      call_once( inited, []() {
        instance=new Singleton(); });
      return instance; }
    //altri metodi...
  };
\end{Cpp}